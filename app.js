const select = document.getElementById('razas');
const img = document.getElementById('img')
const datosMal = document.getElementById('error');
document.getElementById('submit').addEventListener('submit', async (e) => {
    e.preventDefault()
    eliminarMError()
    url = 'https://dog.ceo/api/breeds/list'
    try {
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        console.log(resultado)
        mostrarOptions(resultado);
    } catch (error) {
        const p = document.createElement('p')

        p.innerText = 'No se encotraron datos';
        error.appendChild(p)

    }

})
function mostrarOptions(razas) {
    console.log(razas)
    eliminar()
    razas.message.forEach(raza => {
        console.log(raza)
        const op = document.createElement('option');
        op.value = raza;
        op.innerText = raza;
        select.appendChild(op);
    });

}

document.getElementById('CargarRazasanimales').addEventListener('click', () => {
    if (select.value === '') {
        const p = document.createElement('p')

        p.innerText = 'Selecciones una raza o pulse el boton Cargar Razas';
        error.appendChild(p)
        return
    }
    mostarImagen(select.value)
})
select.addEventListener('click', (e) => {
    mostarImagen(e.target.value)
})
async function mostarImagen(raza = 'affenpinscher') {
    const url = `https://dog.ceo/api/breed/${raza}/images/random`
    try {
        const respuesta = await fetch(url)
        const resultado = await respuesta.json();
        mostraIMG(resultado)
    } catch (error) {
        const p = document.createElement('p')

        p.innerText = 'No se encontro la IMAGEN';
        error.appendChild(p)
    }
}
function mostraIMG(dato) {
    eliminarImg();
    const imgen = document.createElement('img');
    imgen.src = dato.message
    img.appendChild(imgen)
}
function eliminar() {
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
}
function eliminarImg() {
    while (img.firstChild) {
        img.removeChild(img.firstChild);
    }
}
function eliminarMError() {
    while (datosMal.firstChild) {
        datosMal.removeChild(datosMal.firstChild);
    }
}
